import React, {useState} from 'react'
import {Fetcher, FetcherProvider} from './fetcher/Fetcher'

const j = o => JSON.stringify(o, null, 2)

function Test() {
  const [title, setTitle] = useState('')
  const [body, setBody] = useState('')
  const [userId, setUserId] = useState('')
  return (
    <div>
      <Fetcher
        variables={[{title, body, userId}]}
        requestOptions={body => ({url: `posts`, method: 'POST', body})}
      >
        {({fetcherHandlar: sendReq, data, loading, firstLoading}) => (
          <div>
            <form
              onSubmit={e => {
                e.preventDefault()
                sendReq()
              }}
            >
              <input value={title} onChange={e => setTitle(e.target.value)} />
              <br />
              <input value={body} onChange={e => setBody(e.target.value)} />
              <br />
              <input value={userId} onChange={e => setUserId(e.target.value)} />
              <br />
              <button>submit</button>
            </form>
            {firstLoading ? (
              loading ? (
                <h1>Loading</h1>
              ) : (
                <pre>{j(data)}</pre>
              )
            ) : (
              <h1>nodata</h1>
            )}
          </div>
        )}
      </Fetcher>
    </div>
  )
}

function App() {
  return (
    <FetcherProvider
      defultOptions={{
        baseUrl: 'https://jsonplaceholder.typicode.com/',
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
        },
      }}
    >
      <h1>Hoola</h1>
      <hr />
      <Test />
    </FetcherProvider>
  )
}

export default App
