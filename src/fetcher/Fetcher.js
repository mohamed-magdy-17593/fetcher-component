import React, {useContext, useReducer, useEffect, useRef} from 'react'
import * as R from 'ramda'
import createFetcher from './utils/fetcher'
import {isFunction} from './utils/js'

const FetcherContext = React.createContext(createFetcher())

function FetcherProvider({children, defultOptions = {}}) {
  return (
    <FetcherContext.Provider value={createFetcher(defultOptions)}>
      {children}
    </FetcherContext.Provider>
  )
}

/**
 * custom hooks
 */
function useReactSetState(initialState = {}) {
  return useReducer(
    (oldState, mergedState) => ({...oldState, ...mergedState}),
    initialState,
  )
}

function useEffectOnMount(cb) {
  return useEffect(cb, [])
}

function useEffectOnUpdate(cb, variables) {
  const mountedRef = useRef(true)
  return useEffect(() => {
    if (!mountedRef.current) {
      return cb()
    }
    mountedRef.current = false
  }, variables)
}

function useReactSafeSetState(initialState) {
  const [state, setState] = useReactSetState(initialState)
  const mounted = useRef(false)
  useEffectOnMount(() => {
    mounted.current = true
    return () => (mounted.current = false)
  })
  const safeSetState = (...args) => mounted.current && setState(...args)
  return [state, safeSetState]
}

function useFetcher({
  requestOptions = {},
  onSuccess = data => data,
  onError = error => Promise.reject(error),
  variables = [],
  initialdata = null,
  runOnMount = false,
  runOnVariablesUpdate = false,
}) {
  const [state, setState] = useReactSafeSetState({
    data: initialdata,
    error: null,
    loading: false,
    firstLoading: false,
  })

  const fetcher = useContext(FetcherContext)
  const fetcherHandlar = (...args) => {
    setState({loading: true, firstLoading: true})
    return fetcher(
      isFunction(requestOptions)
        ? requestOptions(...(R.isEmpty(args) ? variables : args))
        : requestOptions,
    )
      .then(onSuccess, onError)
      .then(data => {
        setState({data, error: null, loading: false})
        return data
      })
      .catch(error => {
        setState({error, data: null, loading: false})
        throw error
      })
  }

  useEffectOnMount(() => {
    runOnMount && fetcherHandlar()
  })
  useEffectOnUpdate(() => {
    runOnVariablesUpdate && fetcherHandlar()
  }, variables)

  return {
    ...state,
    fetcher,
    fetcherHandlar,
  }
}

const Fetcher = ({children, ...restProps}) => children(useFetcher(restProps))

const withFetcher = R.curry((fetcherProps, WrapComponent, props) => (
  <Fetcher {...fetcherProps}>
    {(...fetcherArgs) => <WrapComponent {...fetcherArgs} {...props} />}
  </Fetcher>
))

export {FetcherProvider, Fetcher, useFetcher, withFetcher}
