import * as R from 'ramda'

export const isFunction = R.is(Function)

export const isString = R.is(String)

export const isObject = R.both(
  R.is(Object),
  R.compose(
    R.not,
    isFunction,
  ),
)

export const callIfFunction = R.ifElse(isFunction, R.call, R.identity)

export const callFunctionsDeep = R.map(
  R.ifElse(isObject, o => callFunctionsDeep(o), callIfFunction),
)
