import * as R from 'ramda'
import {callFunctionsDeep, isString} from './js'

const getUrl = R.curry((baseUrl, url) =>
  /^https?:\/\//i.test(url) ? url : `${baseUrl}${url}`,
)

const serializeRequest = ({body, ...o}) => ({
  ...o,
  ...(body && {body: R.is(FormData, body) ? body : JSON.stringify(body)}),
})

function createFetcher(defaultOptions = {}) {
  return function fetcher(options) {
    const {baseUrl = '', ...defaultRequestOptions} = callFunctionsDeep(
      defaultOptions,
    )
    const {url = '', ...requestOptions} = isString(options)
      ? {url: options}
      : callFunctionsDeep(options)
    return fetch(
      getUrl(baseUrl, url),
      serializeRequest(R.mergeDeepRight(defaultRequestOptions, requestOptions)),
    ).then(res => (res.ok ? res.json() : Promise.reject(new Error(res))))
  }
}

export default createFetcher
export {createFetcher, getUrl}
